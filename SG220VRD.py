import sys
from os import system
import pymodbus
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
client= ModbusClient(method = "rtu", port="/dev/ttyUSB1",stopbits = 2, bytesize = 8, parity = 'N', baudrate= 38400)
try:
    command=sys.argv[1]
    #print(command)
except:
    print('No input argument')
"""
Date is a Holding Register with address 0xb04
Time is a Holding Register with address 0xb08
DR01 0x1100

0x613 : Qcoils status
0x612 : Ycoils status
0x60c : Battery test Indicator N01
0x916 : 6 registers for current and voltage of battery

DR01 : Current Circuit Value 0x1100
DR02 : Current Battery Voltage 0x1101
0C03 : Actuall Flow
0x700 : Run/Stop Program
0x04 : M02 Relay for WatchDog

Teco pinsout in Noah project:
I1= Water pulse
I2= Beging water shutoff
I3= Shutoff valve open
I4= shutoff valve closed
I5= purge valve open
I6= purge valve closed
I7= Remote Low Flow test
I8= Restore water supply
A1= Batt Test Sequenc
A2= Current of circuit. Allergo Voltage output come here
A3= voltage on Battery
A4= Voltage 12VDC charger
X1= Reset Circuit
X2= shutoff valve trouble
X3= AC fail
X4= Reserved
"""


#DR01=0x1100
#DR02=0x1101
DR03=0x1102
DR04=0x1103
DR05=0x1104 # flowRate
DR06=0x1105
DR07=0x1106
DR08=0x1107
DR09=0x1108
DR0A=0x1109
NRelays=0x60c

QCoils=0x613
Ycoils=0x612
RunTeco=0x700
FlowRate=0x1104
currentValue=0x1100
batVoltValue=0x1101
measuringVC=0x916 #6 registers measing voltage and current during batt test Counter number 12,13,14
startTestBat=0x60c
watchDog=0x04
batest1=0x609
batest2=0x60a
M1E=0x2000  # will be set when battery test is on stage A
M1F=0x4000  # will be set when battery test is on stage B
M22=0x2     # will be set when battery test is on stage C

#result= client.read_holding_registers(0x613,1,unit=1)
#result= client.read_holding_registers(0x613,1,unit=1)

def add(x,y):
    assert(x<=9), "%s is bigger than 9" % x
    print(x+y)

def getFlowrate():
    try:
        result= client.read_holding_registers(FlowRate,1,unit=1)
        data=result.registers
        return data[0]/10
    except:
        return -1

def getbatVoltValue():
    try:
        result= client.read_holding_registers(batVoltValue,1,unit=1)
        data=result.registers
        return data[0]
    except:
        return -1

def getCurrent():
        try:
            result= client.read_holding_registers(currentValue,1,unit=1)
            data=result.registers
            if(data[0]<1000): #1000 represent +10Amper which is enough to determine whether our current is negative or positive
                return data[0]/100
            else:
                current=65536-data[0]
            return -(current/100)
        except:
            return -51
def getQcoil():
    try:
        result= client.read_holding_registers(QCoils,1,unit=1)
        data=result.registers
        data=format(data[0],'08b')  # make it as 8-bit package
        return data
    except:
        return -1

def getYcoil():
    try:
        result= client.read_holding_registers(Ycoils,1,unit=1)
        data=result.registers
        data=format(data[0],'04b')  # make it as 8-bit package
        return data
    except:
        return -1
def runTeco():
    try:
        result=client.write_register(RunTeco,1,unit=1)
        return 1
    except:
        return -1

def stopTeco():
    try:
        result=client.write_register(RunTeco,0,unit=1)
        return 1
    except:
        return -1

def tecoWatch():
    try:
        result=client.read_holding_registers(watchDog,1,unit=1)
        data=result.registers[0]
        if(data&0x2):
            return 1
        else:
            return -1

    except:
        return -1

def testBat():
    try:
        result= client.read_holding_registers(startTestBat,1,unit=1)
        data=result.registers
        return data[0]
    except:
        return -1

def measeurCV():
    try:
        result= client.read_holding_registers(measuringVC,6,unit=1)
        data=result.registers
        return data
    except:
        return -1

def testBatt():
    try:
        result= client.read_holding_registers(batest1,2,unit=1)
        data=result.registers
        if(data[0]&M1E==M1E):
            return '1,A'
        elif(data[0]&M1F==M1F):
            return '1,B'
        elif(data[1]&M22==M22):
            return '1,C'
        else:
            return 0

        print(hex(data[0]),hex(data[1]))
    except:
        return -1

funcs={'flowRate':getFlowrate,'batVolt':getbatVoltValue,'current':getCurrent,'QCoils':getQcoil,'YCoils':getYcoil,
'run':runTeco,'stop':stopTeco,'Watch':tecoWatch,'batTest':testBat,'measurCV':measeurCV,'isTest':testBatt}

if __name__ == '__main__':
    try:
        output=funcs.get(command)()
        print(output)
    except:
        print('No Commands')
